import random
def guess_number():
    min_num = 1
    max_num = 100
    attempts = 0
    user_input = ''
    guessed_number = random.randint(min_num, max_num)
    
    while True:
        
        user_input = input(f'Ваше число {guessed_number} (да/больше/меньше) ').lower()
        if user_input == 'да':
            attempts += 1
            with open('result.txt', 'a', encoding='utf-8') as file:
                file.write(f'Кол-во попыток - {attempts} \n')
                file.write(f'Загаданное число - {guessed_number} \n')
                print('Ура я угадал')
            break
        
        elif user_input == 'больше':
            attempts += 1
            min_num = guessed_number + 1
            guessed_number = random.randint(min_num, max_num)
        
        elif user_input == 'меньше':
            attempts += 1
            max_num = guessed_number - 1
            guessed_number = random.randint(min_num, max_num)
        
        else:
            print('Введи да, или больше или меньше')
        
        
guess_number()
            
            