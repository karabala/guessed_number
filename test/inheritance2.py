from inheritance1 import Animal

class Dog(Animal):
    def make_sound(self):
        print("Собака лает")

class Cat(Animal):
    def make_sound(self):
        print("Кошка мяучит")
class Bird(Animal):
    def make_sound(self):
        print("Птица чирикает")

animal = Animal()
dog = Dog()
cat = Cat()
bird = Bird()

animal.make_sound()
dog.make_sound()    
cat.make_sound()
bird.make_sound()