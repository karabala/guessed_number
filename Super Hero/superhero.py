class SuperHero:
    people = 'people' 
    
    def __init__(self, name, nickname, superpower, health_points, catchphrase):
        self.name = name
        self.nickname = nickname
        self.superpower = superpower
        self.health_points = health_points
        self.catchphrase = catchphrase

    def display_name(self):
        print(f"Имя героя: {self.name}")

    def double_health(self):
        self.health_points *= 2

    def __repr__(self):
        return f": {self.nickname}, Суперспособность: {self.superpower}, Здоровье: {self.health_points}"

    def __len__(self):
        return len(self.catchphrase)



hero = SuperHero("Человек-паук", "Питер Паркер", "Паутинная стрельба", 100, "С великой силой приходит великая ответственность")

hero.display_name()  

print("Здоровье до увеличения:", hero.health_points)
hero.double_health()  
print("Здоровье после увеличения:", hero.health_points)

print(hero)  

print("Длина коронной фразы героя:", len(hero.catchphrase)) 