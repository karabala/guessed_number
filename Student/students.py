class Person:
    def __init__(self,fullname, age,is_merried):
        self.fullname = fullname
        self.age = age
        self.is_married = is_merried
    
    def introduce_myself(self):
        print(f'Полное имя:{self.fullname}, возраст: {self.age}, женат или замужем: {self.is_married}')

class Student(Person):
    def __init__(self, fullname, age, is_merried, marks):
        super().__init__(fullname,age,is_merried)
        self.marks = marks
    
    def introduce_myself(self):
        super().introduce_myself()
        print('Оценки: ')
    
        for subject, mark in self.marks.items():
            print(f'{subject}: {mark}')
    
    def calculate_marks(self):
        total_marks = sum(self.marks.values())      
        average_mark = total_marks / len(self.marks)
        print(f'Средний балл: {round(average_mark, 2)}\n')
    
class Teacher(Person):
    def __init__(self, fullname, age, is_married, experience, base_salary):
        super().__init__(fullname, age, is_married)
        self.experience = experience
        self.base_salary = base_salary
    
    def salary_calculation(self):
        for year in range(1, self.experience + 1):
            if year > 3: 
                bonus = self.base_salary * 0.05
                self.base_salary += bonus
                self.base_salary = round(self.base_salary, 2)

        return self.base_salary

           
def create_students():
    students = []
    student1 = students.append(Student("Alice", 20, False, {"Math": 90, "Science": 85, "History": 92}))
    student2 = students.append(Student("Bob", 21, False, {"Math": 75, "Science": 80, "History": 85}))
    student3 = students.append(Student("Charlie", 22, True, {"Math": 85, "Science": 92, "History": 88}))      
    return students

teacher = Teacher("John Doe", 35, True, 6, 5000)

print("\nИнфо Препода\n")
teacher.introduce_myself()
print(f'Опыт работы: {teacher.experience} - лет ')
print(f'Базовая зарплата: {teacher.base_salary}')
print(f'Расчетная зарплата: {teacher.salary_calculation()}')
print('')
print("\nИнфо студентов \n")
students = create_students()
for student in students:
    student.introduce_myself()
    student.calculate_marks()
    
